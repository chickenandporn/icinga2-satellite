#!/bin/bash

sleep 2 

mkdir -p /var/lib/icinga2/certs
chown nagios:nagios /var/lib/icinga2/certs

icinga2 pki save-cert \
  --host $ICINGA2_MASTER1_IP \
  --port 5665 \
  --key /var/lib/icinga2/certs/$HOSTNAME.key \
  --cert /var/lib/icinga2/certs/$HOSTNAME.crt \
  --trustedcert /var/lib/icinga2/certs/trusted-parent.crt

icinga2 node setup \
  --zone $ICINGA2_LOCALZONE \
  --parent_zone $ICINGA2_PARENTZONE \
  --parent_host $ICINGA2_MASTER1 \
  --ticket $ICINGA2_TICKET \
  --trustedcert /var/lib/icinga2/certs/trusted-parent.crt \
  --endpoint $ICINGA2_MASTER1,$ICINGA2_MASTER1_IP \
  --endpoint $ICINGA2_MASTER2,$ICINGA2_MASTER2_IP \
  --accept-config \
  --accept-commands \
  --disable-confd

  sleep 5

touch /tmp/configured
