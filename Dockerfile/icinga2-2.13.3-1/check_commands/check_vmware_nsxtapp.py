#!/usr/bin/python3
#
SCRIPTNAME= "nsxtappicinga"
# Author	Lukas Rueckerl 
# mailto 	info@lkrkl.dev 
VERSION=	"0.1 / 150623"
#
# Tested with Python 3.11
#
# Aggregates monitoring for the NSX Application Platform - intended for Icinga2
# Be sure to install the "requests"-Pip-Package beforehand


################################################################################
# Import and preset variables 

import argparse
import json
import sys
import decimal
from tokenize import String
import requests
import urllib.parse
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning # type: ignore
requests.packages.urllib3.disable_warnings(InsecureRequestWarning) # type: ignore

################################################################################
# Define global Variables
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog=SCRIPTNAME+' | '+VERSION+' | CC-BY',)

sp = p.add_subparsers (dest = 'check', title = "checks")
clusterstatus_p = sp.add_parser ('clusterstatus', help = 'Returns your current cluster status and will exit with Critical Status if not in a stable state')
nodestatus_p = sp.add_parser ('nodestatus', help = 'Checks all nodes in the cluster for resource and control plane availability')
servicestatus_p = sp.add_parser ('servicestatus', help = 'Checks all nodes in the cluster for resource and control plane availability')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of your NSX-T Manager VIP")
p.add_argument ('-username', help = "API-User")
p.add_argument ('-password', help = "API-User's Password")

################################################################################
# Check functions

def call_api_get (host,uri, username, password):
	url = "https://"+host+uri
	req = requests.get(url, auth=HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def call_api_post (host,uri, username, password, payload):	
	url = "https://"+host+uri
	headers = {}

	req = requests.post (url, data = payload, headers = headers, auth = HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def get_clusterStatus (host, uri, username, password):
	ret_code = OK
	msg = "OK"
	perf_data = []

	response = call_api_get(host,uri,username,password)

	if (response["health"]) != "UP":
		ret_code = CRITICAL	
		msg = "Cluster Status is "+str(response["health"])

	if (response["health"]) == "DEGRADED":
		ret_code = WARNING	
		msg = "Cluster Status is "+str(response["health"])

	if ret_code == OK:
		msg = "OK"

	return (ret_code, msg, perf_data)

def get_nodeStatus (host, uri, username, password):
	ret_code = OK
	msg = ""
	perf_data = []

	upcount = 0
	downcount = 0

	response = call_api_get(host,uri,username,password)["data"]

	for node in response:
		if node["health"] != "UP":
			ret_code = WARNING	
			msg += node["node_name"]+" is "+node["health"]
			downcount +=1
		else:
			upcount += 1

	if ret_code == WARNING:
		msg = str(upcount)+" running nodes present, "+str(downcount)+" down: "+msg

	if downcount == 0:
		ret_code = OK
		msg = str(upcount)+" Nodes present and healthy"
	
	if upcount == 0:
		ret_code = CRITICAL
		msg = str(upcount)+" running nodes present, "+str(downcount)+" down"


	return (ret_code, msg, perf_data)

def get_serviceStatus (host, uri, username, password):
	ret_code = OK
	msg = "OK"
	perf_data = []

	upcount = 0
	downcount = 0

	response = call_api_get(host,uri,username,password)["data"]

	for service in response:
		if service["health"] != "UP":
			ret_code = WARNING	
			msg += service["service_name"]+" is "+service["health"]+", "
			downcount +=1
			perf_data.append("'"+(service["service_name"])+"'=1;1;1;0;1")
		else:
			upcount += 1
			perf_data.append("'"+(service["service_name"])+"'=0;1;1;0;1")

		

	if ret_code == WARNING:
		msg = str(upcount)+" running services present, "+str(downcount)+" down or only partially up: "+msg[:-2]

	if downcount == 0:
		ret_code = OK
		msg = str(upcount)+" services present and healthy"
	
	if upcount == 0:
		ret_code = CRITICAL
		msg = str(upcount)+" running services present, "+str(downcount)+" down"


	return (ret_code, msg, perf_data)



################################################################################
# API Endpoint definition

checks = {
	'clusterstatus' : {
		'api_path' : "/napp/api/v1/platform/monitor/platform/status",
		'function' : get_clusterStatus
	},
	'nodestatus' : {
		'api_path' : "/napp/api/v1/platform/monitor/nodes/status",
		'function' : get_nodeStatus
	},
	'servicestatus' : {
		'api_path' : "/napp/api/v1/platform/monitor/services/status",
		'function' : get_serviceStatus
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (str(args.host), checks[args.check]['api_path'], args.username, args.password)

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += " | " + " ".join(perf_data)

print (msg.rstrip ())
sys.exit (ret_code)

