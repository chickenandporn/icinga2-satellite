#!/usr/bin/python3
#
# Author	Lukas Rueckerl 
# mailto 	lukas.rueckerl@edv-bv.de
# 
version = "0/15072022"
#
# Tested with Python 3.10.2
#


################################################################################
# Import and preset variables 

from email import message
from email.errors import MessageDefect
import re
from pysnmp import hlapi
import argparse
import sys

# Possible Exit codes
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog='nutanix2icinga | Version '+version+' | ©edv-bv GmbH',)

sp = p.add_subparsers (dest = 'check', title = "checks")

# Check for alerts
get_p = sp.add_parser ('clusterstatus', help = 'Gets an OID value')
get_p = sp.add_parser ('clusterusedstorage', help = 'Gets an OID value')
get_p = sp.add_parser ('clustercpuusage', help = 'Gets an OID value')
get_p = sp.add_parser ('clustermemoryusage', help = 'Gets an OID value')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of the system")
p.add_argument ('-user', help = "SNMPv3 User")
p.add_argument ('-privkey', help = "SNMPv3 Priv-Password")
p.add_argument ('-authkey', help = "SNMPv3 Auth-Password")
p.add_argument ('-privprot', help = "SNMPv3 Priv-Protocol")
p.add_argument ('-authprot', help = "SNMPv3 Auth-Protocol")

################################################################################

# Define Functions

def construct_object_types(list_of_oids):
    object_types = []
    for oid in list_of_oids:
        object_types.append(hlapi.ObjectType(hlapi.ObjectIdentity(oid)))
    return object_types


def construct_value_pairs(list_of_pairs):
    pairs = []
    for key, value in list_of_pairs.items():
        pairs.append(hlapi.ObjectType(hlapi.ObjectIdentity(key), value))
    return pairs


def get(target, oids, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.getCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_object_types(oids)
    )
    return fetch(handler, 1)[0]


def set(target, value_pairs, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.setCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_value_pairs(value_pairs)
    )
    return fetch(handler, 1)[0]


def get_bulk(target, oids, credentials, count, start_from=0, port=161,
             engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.bulkCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        start_from, count,
        *construct_object_types(oids)
    )
    return fetch(handler, count)


def get_bulk_auto(target, oids, credentials, count_oid, start_from=0, port=161,
                  engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    count = get(target, [count_oid], credentials, port, engine, context)[count_oid]
    return get_bulk(target, oids, credentials, count, start_from, port, engine, context)


def cast(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        try:
            return float(value)
        except (ValueError, TypeError):
            try:
                return str(value)
            except (ValueError, TypeError):
                pass
    return value


def fetch(handler, count):
    result = []
    for i in range(count):
        try:
            error_indication, error_status, error_index, var_binds = next(handler)
            if not error_indication and not error_status:
                items = {}
                for var_bind in var_binds:
                    items[str(var_bind[0])] = cast(var_bind[1])
                result.append(items)
            else:
                raise RuntimeError('Got SNMP error: {0}'.format(error_indication))
        except StopIteration:
            break
    return result

def get_clusterstatus(host, oid, user, authkey, privkey, authprot, privprot):
	ret_code = OK
	msg = ""
	perf_data = []
	value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
	msg = str(value)
	if str(value) == str("stopped"):
		ret_code = CRITICAL
	return (ret_code, msg, perf_data)

def get_clustertotalstorage(host, oid, user, authkey, privkey, authprot, privprot, testvalue=False):
	ret_code = OK
	msg = ""
	perf_data = []
	value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
	msg = str(value)
	if testvalue==True:
		return value
	else:
		return (ret_code, msg, perf_data)

def get_clusterusedstorage(host, oid, user, authkey, privkey, authprot, privprot):
	ret_code = OK
	msg = ""
	perf_data = []
	value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
	value_total = get_clustertotalstorage (host, ["1.3.6.1.4.1.41263.504.0"], user, authkey, privkey, authprot, privprot, True)
	value = (int(value)*100)/int(value_total)
	if int(value) > 70:
		ret_code = WARNING
	if int(value) > 80: 
		ret_code = CRITICAL
	msg="Total Storage Usage: "+str(value)+"%"
	return (ret_code, msg, perf_data)

def get_clustermemoryusage (host, oid, user, authkey, privkey, authprot, privprot):
	ret_code = OK
	msg = ""
	perf_data = []
	end = False
	i = 1
	baseOidNodes = "1.3.6.1.4.1.41263.9.1.1."
	nodes = []

	while end == False:
		oid = [str(baseOidNodes)+str(i)]
		value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
		if value != "":
			nodes.append(int(value))
		else:
			end = True
		i = i+1

	baseOidNodes = "1.3.6.1.4.1.41263.9.1.8."
	total_memory = 0
	for node in nodes:
		oid = [str(baseOidNodes)+str(node)]
		value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
		total_memory += int(value)

	memusagecluster = round(total_memory/len(nodes),2)

	if memusagecluster > 80:
		ret_code = WARNING
	if memusagecluster > 90:
		ret_code = CRITICAL

	msg = "Total Memory Usage: "+str(memusagecluster)+"%"
	return (ret_code, msg, perf_data)
	
def get_clustercpuusage (host, oid, user, authkey, privkey, authprot, privprot):
	ret_code = OK
	msg = ""
	perf_data = []
	end = False
	i = 1
	baseOidNodes = "1.3.6.1.4.1.41263.9.1.1."
	nodes = []

	while end == False:
		oid = [str(baseOidNodes)+str(i)]
		value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
		if value != "":
			nodes.append(int(value))
		else:
			end = True
		i = i+1

	baseOidNodes = "1.3.6.1.4.1.41263.9.1.6."
	total_cpu = 0
	for node in nodes:
		oid = [str(baseOidNodes)+str(node)]
		value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
		total_cpu += int(value)

	cpuusagecluster = round(total_cpu/len(nodes),2)

	if cpuusagecluster > 80:
		ret_code = WARNING
	if cpuusagecluster > 90:
		ret_code = CRITICAL

	msg = "Total CPU Usage: "+str(cpuusagecluster)+"%"
	return (ret_code, msg, perf_data)

checks = {
	'clusterstatus' : {
		'oid' : ["1.3.6.1.4.1.41263.503.0"],
		'function' : get_clusterstatus
	},
	'clusterusedstorage' : {
		'oid' : ["1.3.6.1.4.1.41263.505.0"],
		'function' : get_clusterusedstorage
	},
	'clustertotalstorage' : {
		'oid' : ["1.3.6.1.4.1.41263.504.0"],
		'function' : get_clustertotalstorage
	},
	'clustercpuusage' : {
		'oid' : ["1.3.6.1.4.1.41263.9.1.6"],
		'function' : get_clustercpuusage
	},
	'clustermemoryusage' : {
		'oid' : ["1.3.6.1.4.1.41263.9.1.6"],
		'function' : get_clustermemoryusage
	}
}

authprot = {
	'MD5' : hlapi.usmHMACMD5AuthProtocol,
	'SHA' : hlapi.usmHMACSHAAuthProtocol,
	'SHA-256' : hlapi.usmHMAC256SHA384AuthProtocol,
	'SHA-512' : hlapi.usmHMAC384SHA512AuthProtocol

}

privprot = {
	'DES' : hlapi.usmDESPrivProtocol,
	'AES' : hlapi.usmAesCfb128Protocol,
	'AES-192' : hlapi.usmAesCfb192Protocol,
	'AES-256' : hlapi.usmAesCfb256Protocol

}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (args.host, checks[args.check]['oid'] , args.user, args.authkey , args.privkey, authprot[args.authprot], privprot[args.privprot])

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)
