#!/usr/bin/python3
#
# Author	Lukas Rueckerl 
# mailto 	lukas.rueckerl@edv-bv.de
# 
# Version	1/24012023
#
# Tested with Python 3.9.13
#
# Aggregates metrics from the xClarity API for monitoring - intended for Icinga2
#


################################################################################
# Import and preset variables 

import argparse
import json
import sys
import decimal
from tokenize import String
import requests
import urllib.parse
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import redfish
import traceback
import lenovo_utils as utils


# Possible Exit codes
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3


################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog='xClarity2icinga / Version 1/24012023 / ©edv-bv GmbH',)

sp = p.add_subparsers (dest = 'check', title = "checks")

raid_health_p = sp.add_parser ('raid_health', help = 'Returns Health state of RAID controller, logical volumes and disks')
psu_health_p = sp.add_parser ('psu_health', help = 'Returns PSU State, Health and Load')
cpu_health_p = sp.add_parser ('cpu_health', help = 'Returns CPU Model and Health')
memory_health_p = sp.add_parser ('memory_health', help = 'Returns present Memory Modules, installed Capacity and absent Slots. If DIMMs report hardware failures, the sensor is in critical state and returns the names of the faulty modules')
thermic_health_p = sp.add_parser ('thermic_health', help = 'Returns temperatur readings in °C for Inlet, Outlet, CPU1 and CPU2')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of the xClarity interface")
p.add_argument ('-user', help = "User")
p.add_argument ('-passw', help = "Password")

################################################################################
# Method definition

def create_api_session (ip, login_account, login_password, system_id = ""):
	login_host = "https://" + ip
	try:
        # Connect using the BMC address, account name, and password
        # Create a REDFISH object
		REDFISH_OBJ = redfish.redfish_client(base_url=login_host, username=login_account, timeout=utils.g_timeout,
                                             password=login_password, default_prefix='/redfish/v1', cafile=utils.g_CAFILE)
        # Login into the server and create a session
		REDFISH_OBJ.login(auth=utils.g_AUTH)		
		return (REDFISH_OBJ)

	except:
		traceback.print_exc()
		return -1

    

def destroy_api_session(REDFISH_OBJ):
	REDFISH_OBJ.logout()
			

def raid_health(REDFISH_OBJ):
	ret_code = OK
	msg = ""
	perf_data = []

	# GET the ComputerSystem resource
	system = utils.get_system_url("/redfish/v1", "", REDFISH_OBJ)
    
	if not system:
		result = {'ret': False, 'msg': "This system id is not exist or system member is None"}
		REDFISH_OBJ.logout()
		return -1

	for i in range(len(system)):
		system_url = system[i]
		response_system_url = REDFISH_OBJ.get(system_url, None)
		if response_system_url.status == 200:
			if "Storage" in response_system_url.dict:
				storage_url = response_system_url.dict["Storage"]["@odata.id"]
			else:
				storage_url = response_system_url.dict["SimpleStorage"]["@odata.id"]
			response_storage_url = REDFISH_OBJ.get(storage_url, None)
			storage_count = len(response_storage_url.dict["Members"])
			for nic in range(0, storage_count):
				storage_x_url = response_storage_url.dict["Members"][nic]["@odata.id"]
				response_storage_x_url = REDFISH_OBJ.get(storage_x_url, None)
				if response_storage_x_url.status == 200:

					msg = "Controller: "+response_storage_x_url.dict["Status"]["Health"]
					if response_storage_x_url.dict["Status"]["Health"] != "OK":
						ret_code = CRITICAL

					drive_list = []
					if "Drives" in response_storage_x_url.dict:
						msg += " / "+str(len(response_storage_x_url.dict["Drives"]))+" Disks"
						for disk in response_storage_x_url.dict["Drives"]:
							disk_inventory = {}
							disk_url = disk["@odata.id"]
							response_disk_url = REDFISH_OBJ.get(disk_url, None)
							if response_disk_url.status == 200:						

								for key in response_disk_url.dict:
									if key not in ["Description", "@odata.context", "@odata.id", "@odata.type",
													"@odata.etag", "Links", "Actions", "RelatedItem"]:
										disk_inventory[key] = response_disk_url.dict[key]
								drive_list.append(disk_inventory)	

								name = response_disk_url.dict["Id"]
								mediaType = response_disk_url.dict["MediaType"]
								enabled = response_disk_url.dict["Status"]["State"]
								health = response_disk_url.dict["Status"]["Health"]

								if health != "OK":
									ret_code = CRITICAL
									msg += ", "+name+" CRITICAL, State: "+health+", "+enabled+", Media: "+mediaType
								else: 
									msg += ", "+name+" "+health	
								

					if "Volumes" in response_storage_x_url.dict:
						volumes_url = response_storage_x_url.dict["Volumes"]["@odata.id"]
						response_volumes_url = REDFISH_OBJ.get(volumes_url, None)
						if response_volumes_url.status == 200:
							volumes_list = []
							
							for volume in response_volumes_url.dict["Members"]:
								volume_inventory = {}
								volume_url = volume["@odata.id"]
								response_volume_url = REDFISH_OBJ.get(volume_url, None)
								if response_volume_url.status == 200:
									for key in response_volume_url.dict:
										if key not in ["Description", "@odata.context", "@odata.id", "@odata.type",
														"@odata.etag", "Links", "Actions", "RelatedItem"]:
											volume_inventory[key] = response_volume_url.dict[key]
										if key == "Links":
											drivesIds = []
											if "Drives" in response_volume_url.dict[key]:
												for drive in response_volume_url.dict[key]["Drives"]:
													drivesIds.append(drive["@odata.id"].split("/")[-1])  
											volume_inventory["LinkedDriveIds"] = drivesIds
									volumes_list.append(volume_inventory)

							msg += " / "+str(len(volumes_list))+" Volumes"
							for volume in volumes_list:
								if volume["Status"]["Health"] != "OK":
									ret_code = CRITICAL
									msg += ", "+volume["Name"]+" CRITICAL, "+volume["Status"]["Health"]
								else:
									msg += ", "+volume["Name"]+" "+volume["Status"]["Health"]
		else:
			result = {'ret': False, 'msg': "response_system_url Error code %s" % response_system_url.status}
			REDFISH_OBJ.logout()
			return result

	destroy_api_session(REDFISH_OBJ)
	return (ret_code, msg, perf_data)
                        
def psu_health(REDFISH_OBJ):
	ret_code = OK
	msg = ""
	perf_data = []
	psu_details = []
	response_base_url = REDFISH_OBJ.get('/redfish/v1', None)
	if response_base_url.status == 200:
		chassis_url = response_base_url.dict['Chassis']['@odata.id']
	else:
		error_message = utils.get_extended_error(response_base_url)
		result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
			'/redfish/v1', response_base_url.status, error_message)}
		return result
	response_chassis_url = REDFISH_OBJ.get(chassis_url, None)
	if response_chassis_url.status == 200:
		for request in response_chassis_url.dict['Members']:
			request_url = request['@odata.id']
			response_url = REDFISH_OBJ.get(request_url, None)
			if response_url.status == 200:
				# if chassis is not normal skip it
				if len(response_chassis_url.dict['Members']) > 1 and ("Links" not in response_url.dict or "ComputerSystems" not in response_url.dict["Links"]):
					continue
				if 'PowerSubsystem' in response_url.dict:
					# Get the powersubsystem resources
					powersubsystem_url = response_url.dict['PowerSubsystem']['@odata.id']
					response_powersubsystem_url = REDFISH_OBJ.get(powersubsystem_url, None)
					if response_powersubsystem_url.status == 200:
						if 'PowerSupplies' not in response_powersubsystem_url.dict:
							result = {'ret': False, 'msg': "There is no PowerSupplies data in %s" % powersubsystem_url}
							REDFISH_OBJ.logout()
							return result
						# Get PowerSupplies resources
						powersupplies_url = response_powersubsystem_url.dict['PowerSupplies']['@odata.id']
						response_powersupplies_url = REDFISH_OBJ.get(powersupplies_url, None)
						for i in range(response_powersupplies_url.dict["Members@odata.count"]):
							members_url = response_powersupplies_url.dict['Members'][i]['@odata.id']
							response_members_url = REDFISH_OBJ.get(members_url, None)
							psu = response_members_url.dict
							for property in ["@odata.id", "@odata.context", "@odata.type", "@odata.etag"]:
								if property in psu:
									del psu[property]
							if 'Metrics' in response_members_url.dict:
								# Get Metrics resources of each PSU
								metrics_url = response_members_url.dict['Metrics']['@odata.id']
								response_metrics_url = REDFISH_OBJ.get(metrics_url, None)
								metrics = response_metrics_url.dict
								for property in ["@odata.id", "@odata.context", "@odata.type", "@odata.etag"]:
									if property in metrics:
										del metrics[property]
								psu["Metrics"] = metrics
							psu_details.append(psu)
					else:
						error_message = utils.get_extended_error(response_powersubsystem_url)
						result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
							powersubsystem_url, response_powersubsystem_url.status, error_message)}
						return result
				else:
					# Get the power resources
					power_url = response_url.dict['Power']['@odata.id']
					response_power_url = REDFISH_OBJ.get(power_url, None)
					if response_power_url.status == 200:
						if 'PowerSupplies' not in response_power_url.dict:
							result = {'ret': False, 'msg': "There is no PowerSupplies data in %s" % power_url}
							REDFISH_OBJ.logout()
							return result
						power_supply_list = response_power_url.dict['PowerSupplies']
						for PowerSupplies in power_supply_list:
							entry = {}
							for property in ['Name', 'SerialNumber', 'PowerOutputWatts', 'EfficiencyPercent', 'LineInputVoltage',
								'PartNumber', 'FirmwareVersion', 'PowerCapacityWatts', 'PowerInputWatts', 'Model',
								'PowerSupplyType', 'Status', 'Manufacturer', 'HotPluggable', 'LastPowerOutputWatts',
								'InputRanges', 'LineInputVoltageType', 'Location']:
								if property in PowerSupplies:
									entry[property] = PowerSupplies[property]
							if 'Oem' in PowerSupplies and 'Lenovo' in PowerSupplies['Oem']:
								entry['Oem'] = {'Lenovo':{}}
								for oemprop in ['FruPartNumber', 'ManufactureDate', 'ManufacturerName']:
									if oemprop in PowerSupplies['Oem']['Lenovo']:
										entry['Oem']['Lenovo'][oemprop] = PowerSupplies['Oem']['Lenovo'][oemprop]
							psu_details.append(entry)
					else:
						error_message = utils.get_extended_error(response_power_url)
						result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
							power_url, response_power_url.status, error_message)}
						return result
			else:
				error_message = utils.get_extended_error(response_url)
				result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
					request_url, response_url.status, error_message)}
				return result
	else:
		error_message = utils.get_extended_error(response_chassis_url)
		result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
			chassis_url, response_chassis_url.status, error_message)}
		return result

	msg = str(len(psu_details))+" PSUs"

	for psu in psu_details:
		if str(psu["Status"]["Health"]) != "OK":
			ret_code = CRITICAL
			msg += " / CRITICAL:"+str(psu["Name"])+" STATE:"+str(psu["Status"]["State"])+" HEALTH:"+str(psu["Status"]["Health"])
		elif (int(psu["LastPowerOutputWatts"]) / int(psu["InputRanges"][0]["OutputWattage"])*100) > 90:
			ret_code = WARNING
			msg += " / OVERLOAD-WARNING:"+str(psu["Name"])+", Health "+str(psu["Status"]["Health"])+", Input:"+str(psu["LineInputVoltage"])+"V, Load:"+str(int(psu["LastPowerOutputWatts"]) / int(psu["InputRanges"][0]["OutputWattage"])*100)+"% ("+str(psu["LastPowerOutputWatts"])+"W/"+str(psu["InputRanges"][0]["OutputWattage"])+"W)"
		else:
			msg += " / "+str(psu["Name"])+", Health "+str(psu["Status"]["Health"])+", Input:"+str(psu["LineInputVoltage"])+"V, Load:"+str(int(psu["LastPowerOutputWatts"]) / int(psu["InputRanges"][0]["OutputWattage"])*100)+"% ("+str(psu["LastPowerOutputWatts"])+"W/"+str(psu["InputRanges"][0]["OutputWattage"])+"W)"

	destroy_api_session(REDFISH_OBJ)
	return (ret_code, msg, perf_data)

def cpu_health(REDFISH_OBJ):
	ret_code = OK
	msg = ""
	perf_data = []

	cpu_details = []
	# GET the ComputerSystem resource
	system = utils.get_system_url("/redfish/v1", "", REDFISH_OBJ)
	if not system:
		result = {'ret': False, 'msg': "This system id is not exist or system member is None"}
		REDFISH_OBJ.logout()
		return result

	for i in range(len(system)):
		# Get Processors url
		system_url = system[i]
		response_system_url = REDFISH_OBJ.get(system_url, None)
		if response_system_url.status == 200:
			processors_url = response_system_url.dict['Processors']['@odata.id']
		else:
			result = {'ret': False, 'msg': "response_system_url Error code %s" % response_system_url.status}
			REDFISH_OBJ.logout()
			return result

		# Get the Processors collection
		response_processors_url = REDFISH_OBJ.get(processors_url, None)
		if response_processors_url.status == 200:
			# Get Members url
			members_count = response_processors_url.dict['Members@odata.count']
		else:
			result = {'ret': False, 'msg': "response_processors_url Error code %s" % response_processors_url.status}
			REDFISH_OBJ.logout()
			return result

		# Get each processor info
		for i in range(members_count):
			cpu = {}
			# Get members url resource
			members_url = response_processors_url.dict['Members'][i]['@odata.id']
			response_members_url = REDFISH_OBJ.get(members_url, None)
			if response_members_url.status == 200:
				for property in ['Id', 'Name', 'TotalThreads', 'InstructionSet', 'Status', 'ProcessorType', 'ProcessorId', 'ProcessorMemory', 
					'ProcessorArchitecture', 'TotalCores', 'TotalEnabledCores', 'Manufacturer', 'MaxSpeedMHz', 'Model', 'Socket', 'TDPWatts']:
					if property in response_members_url.dict:
						cpu[property] = response_members_url.dict[property]
				cpu_details.append(cpu)
			else:
				result = {'ret': False, 'msg': "response_members_url Error code %s" % response_members_url.status}
				REDFISH_OBJ.logout()
				return result

	msg = str(len(cpu_details))+"CPUs"
	for cpu in cpu_details:
		if str(cpu["Status"]["Health"]) != "OK":
			ret_code = CRITICAL
			msg += " / CRITICAL:"+str(cpu["Socket"])+" ("+str(cpu["Model"])+"), Health:"+str(cpu["Status"]["Health"])+", State:"+str(cpu["Status"]["State"])
		else:
			msg += " / "+str(cpu["Socket"])+" ("+str(cpu["Model"])+"), Health:"+str(cpu["Status"]["Health"])+", State:"+str(cpu["Status"]["State"])


	destroy_api_session(REDFISH_OBJ)
	return (ret_code, msg, perf_data)

def memory_health(REDFISH_OBJ):
	ret_code = OK
	msg = ""
	perf_data = []
	member_id = None
	list_memory_info = []

	system = utils.get_system_url("/redfish/v1", "", REDFISH_OBJ)
	if not system:
		result = {'ret': False, 'msg': "This system id is not exist or system member is None"}
		return result
	
	for i in range(len(system)):
		system_url = system[i]
		response_system_url = REDFISH_OBJ.get(system_url, None)
		if response_system_url.status == 200:
			memroys_url = response_system_url.dict["Memory"]["@odata.id"]
			response_memory_url = REDFISH_OBJ.get(memroys_url,None)
			if response_memory_url.status == 200:
				list_memory_url = response_memory_url.dict["Members"]
				# check member_id validity
				if member_id != None:
					if member_id <= 0 or member_id > len(list_memory_url):
						result = {'ret': False, 'msg': "Specified member id is not valid. The id should be within 1~%s" % (len(list_memory_url))}
						REDFISH_OBJ.logout()
						return result

				# get each memory info
				index = 1
				for memory_dict in list_memory_url:
					if member_id != None and index != member_id:
						index = index + 1
						continue
					index = index + 1
					sub_memory_url = memory_dict["@odata.id"]
					response_sub_memory_url = REDFISH_OBJ.get(sub_memory_url,None)
					if response_sub_memory_url.status == 200:
						memory_info = {}
						if response_sub_memory_url.dict["Status"]["State"] == "Absent":
							memory_info["Status"] = response_sub_memory_url.dict["Status"]
							memory_info["MemoryLocation"] = response_sub_memory_url.dict["MemoryLocation"]
							memory_info["Id"] = response_sub_memory_url.dict["Id"]
							memory_info["Name"] = response_sub_memory_url.dict["Name"]
							list_memory_info.append(memory_info)
							continue
						for key in response_sub_memory_url.dict:
							if key == "Oem":
								continue
							if key not in ["Description", "@odata.context", "@odata.id", "@odata.type",
										"@odata.etag", "Links", "Actions", "RelatedItem"]:
								memory_info[key] = response_sub_memory_url.dict[key]
						list_memory_info.append(memory_info)
					else:
						error_message = utils.get_extended_error(response_sub_memory_url)
						result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
							sub_memory_url, response_sub_memory_url.status, error_message)}
						return result
			else:
				error_message = utils.get_extended_error(response_memory_url)
				result = {'ret': False, 'msg': "Url '%s' response Error code %s\n error_message: %s" % (
					memroys_url, response_memory_url.status, error_message)}
				return result
		else:
			error_message = utils.get_extended_error(response_system_url)
			result = {'ret': False, 'msg': "Url '%s' response Error code %s\n error_message: %s" % (
				system_url, response_system_url.status, error_message)}
			return result

	total_capacity = 0
	total_installed_modules = 0
	total_absent_modules = 0
	err_msg = ""

	for memory in list_memory_info:
		if str(memory["Status"]["Health"]) == "OK" and str(memory["Status"]["State"]) == "Enabled" and memory["CapacityMiB"] > 0:
			total_capacity += memory["CapacityMiB"]
			total_installed_modules += 1
		elif str(memory["Status"]["State"]) == "Absent":
			total_absent_modules += 1
		elif str(memory["Status"]["Health"]) != "OK" and str(memory["Status"]["State"]) != "Absent":
			ret_code = CRITICAL
			err_msg += " / "+str(memory["Name"])+" Health:"+str(memory["Status"]["Health"])

	msg = str(total_installed_modules)+" OK Modules / "+str(total_capacity/1024)+" GiB usable Capacity"+err_msg+" / "+str(total_absent_modules)+" Slots absent"

	destroy_api_session(REDFISH_OBJ)
	return (ret_code, msg, perf_data)

def thermic_health(REDFISH_OBJ):
	ret_code = OK
	msg = ""
	perf_data = []

	response_base_url = REDFISH_OBJ.get('/redfish/v1', None)
	# Get response_base_url
	if response_base_url.status == 200:
		chassis_url = response_base_url.dict['Chassis']['@odata.id']
	else:
		error_message = utils.get_extended_error(response_base_url)
		result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
			'/redfish/v1', response_base_url.status, error_message)}
		return result
	response_chassis_url = REDFISH_OBJ.get(chassis_url, None)
	if response_chassis_url.status == 200:
		rt_list_temperatures = []
		#get temperatures inventory
		for request in response_chassis_url.dict['Members']:
			request_url = request['@odata.id']
			response_url = REDFISH_OBJ.get(request_url, None)
			if response_url.status == 200:
				# if chassis is not normal skip it
				if len(response_chassis_url.dict['Members']) > 1 and ("Links" not in response_url.dict or
						"ComputerSystems" not in response_url.dict["Links"]):
					continue
				if 'ThermalSubsystem' in response_url.dict and '@odata.id' in response_url.dict['ThermalSubsystem']:
					thermal_subsystem_url = response_url.dict["ThermalSubsystem"]['@odata.id']
					response_thermal_url = REDFISH_OBJ.get(thermal_subsystem_url, None)
					if response_thermal_url.status == 200:
						temperatures_url = response_thermal_url.dict["ThermalMetrics"]['@odata.id']
						temperatures_response = REDFISH_OBJ.get(temperatures_url, None)
						for temperature in temperatures_response.dict["TemperatureReadingsCelsius"]:
							tmp_temperatures_item = {}
							data_source_uri = temperature['DataSourceUri']
							readings_celsius = REDFISH_OBJ.get(data_source_uri, None)
							if readings_celsius.status != 200:
								error_message = utils.get_extended_error(readings_celsius)
								result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
												data_source_uri, readings_celsius.status, error_message)}
								return result
							for key in readings_celsius.dict:
								if key not in ["Description", "@odata.context", "@odata.id", "@odata.type",
												"@odata.etag", "Links", "Actions", "RelatedItem"]:
									tmp_temperatures_item[key] = readings_celsius.dict[key]
							rt_list_temperatures.append(tmp_temperatures_item)
					else:
						error_message = utils.get_extended_error(response_thermal_url)
						result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
							thermal_subsystem_url, response_thermal_url.status, error_message)}
						return result
				else:
					if "Thermal" not in response_url.dict:
						continue
					thermal_url = response_url.dict["Thermal"]['@odata.id']
					response_thermal_url = REDFISH_OBJ.get(thermal_url, None)
					if response_thermal_url.status == 200:
						list_temperatures = response_thermal_url.dict["Temperatures"]
						for temperatures_item in list_temperatures:
							tmp_temperatures_item = {}
							for key in temperatures_item:
								if key not in ["Description", "@odata.context", "@odata.id", "@odata.type",
												"@odata.etag", "Links", "Actions", "RelatedItem"]:
									tmp_temperatures_item[key] = temperatures_item[key]
							rt_list_temperatures.append(tmp_temperatures_item)
					else:
						error_message = utils.get_extended_error(response_thermal_url)
						result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
							thermal_url, response_thermal_url.status, error_message)}
						return result
			else:
				error_message = utils.get_extended_error(response_url)
				result = {'ret': False, 'msg': "Url '%s' response Error code %s\nerror_message: %s" % (
					request_url, response_url.status, error_message)}
				return result

		for sensor in rt_list_temperatures:
			if sensor["Name"] == "CPU1 Temp":
				msg += sensor["Name"]+": "+str(sensor["ReadingCelsius"])+"°C / "
			if sensor["Name"] == "CPU2 Temp":
				msg += sensor["Name"]+": "+str(sensor["ReadingCelsius"])+"°C / "
			if sensor["Name"] == "Ambient Temp":
				msg += sensor["Name"]+": "+str(sensor["ReadingCelsius"])+"°C / "
			if sensor["Name"] == "Exhaust Temp":
				msg += sensor["Name"]+": "+str(sensor["ReadingCelsius"])+"°C"
			

	else:
		ret_code = WARNING
		msg = "CANT FETCH DATA"	

	destroy_api_session(REDFISH_OBJ)
	return (ret_code, msg, perf_data)

################################################################################
# API Endpoint definition

checks = {
	'raid_health' : {
		'function' : raid_health,
	},
	'psu_health' : {
		'function' : psu_health,
	},
	'cpu_health' : {
		'function' : cpu_health,
	},
	'memory_health' : {
		'function' : memory_health,
	}	,
	'thermic_health' : {
		'function' : thermic_health,
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()


(ret_code, msg, perf_data) = checks[args.check]['function'] (create_api_session(args.host, args.user, args.passw)) #type:ignore 

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "/ " + " ".join (sorted (perf_data))

print (msg.rstrip ())

sys.exit (ret_code)