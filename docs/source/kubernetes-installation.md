# Kubernetes Installation

## Kubernetes Distribution

In this basic installation fo Kubernetes (K8s) we will install a single node 
cluster with Ranchers K3s. K3s is a lightweight Kubernetes distribution with 
very small integration. The distribution is fully compatible with other K8s 
and its deployments or ressources. To use the icinga2 satellite in K8s there is 
no need to provide persistent volumes and volume claims.

```{hint} 
It is dependent of your Linux distribution which configuration option should be
used, or is necessary. The Quickstart should work for most use cases, bur if you
want to have the full option please visit:
<a href="https://docs.k3s.io/installation/configuration" target="_blank">
    Configuration of installer
</a>
```

To prepare the Linux distribution it is recommended that you create a group and
add a administrative account (who should administer the Kubernetes cluster) to 
this group. If you don't have a user please create one as well.

```shell
groupadd kubernetes

# Add existing user to the group
usermod -a -G kubernetes $UserToAdd

# Create a new user
useradd -m -G kubernetes $UserToCreate
passwd $UserToCreate
```

After the creation or addition of the User you can install the K3s single node
cluster with a single line command. In this case it is necessary to disable 
the *traefik* ingress. *traefik* can not be used with the icinga2 default TCP 
port 5665. In the command there is another option that the *kubeconfig*-File 
will be created with the Read Write access for the owner and the group.

```shell
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --disable traefik --write-kubeconfig-mode 660" sh -s -
```

After the installation process is done, you can change the ownership of the 
configuration file:

```shell
chown root:kubernetes -R /etc/rancher/
```

